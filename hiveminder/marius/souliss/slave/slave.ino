/**************************************************************************
    Souliss - Garage Door
    
    Control a garage door using two Ethernet boards: one device act on the 
    relays that drive the motor and get the limit switches, the other has one 
    push-button for opening and closing the door. The door can be controlled
    also via Android (download SoulissApp from Play Store).
   
	Connect the boards via USART crossing TX or RX, or through an RS485 transceiver.
   
    Ensure to use limit switches to protect the motor once the door is completely
    closed or open, if limit switches are not used the motor should be self-protected.
        
    Run this code on one of the following boards:
      - Arduino with RS485 transceiver
            
***************************************************************************/

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/usart.h"                     // USART RS485

/*************/
// Use the following if you are using an RS485 transceiver with 
// transmission enable pin, otherwise delete this section.
//
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           3
#define USARTDRIVER             Serial
/*************/

// Include framework code and libraries
#include <SPI.h>

/*** All configuration includes should be above this line ***/ 
#include "Souliss.h"

// Define the RS485 network configuration
#define myvNet_subnet   0xFF00
#define Gateway_RS485   0xCE01
#define Peer_RS485      0xCE02

#define MainLedOn 0xF011,0x01
#define MainLedOff 0xF012,0x01

#define GARAGEDOOR_NODE1            0                       
#define GARAGEDOOR_NODE2            0   
#define GARAGELIGHT_NODE2           1

#define INPUTPIN_LIMIT_OPENING 2
#define INPUTPIN_LIMIT_CLOSING 4

#define OUTPUTPIN_LAMP 7
#define OUTPUTPIN_OPENING 5 //8
#define OUTPUTPIN_CLOSING 6 //9

#define USART_DISABLE_PIN 8 // bridge this to ground to disable comms, for in case :)
boolean disableUsart;
#define DEBUG_PIN 9
int readVal = 123;
boolean ledToggle=true;

void setup()
{   
    Initialize();
    
    // Set network parameters
    SetAddress(Peer_RS485, myvNet_subnet, Gateway_RS485);      
/*
    // Set the typical logic to handle the garage door and light
    Set_GarageDoor (GARAGEDOOR_NODE2);
    Set_SimpleLight(GARAGELIGHT_NODE2);
    
    // Define inputs, outputs pins
    pinMode(INPUTPIN_LIMIT_OPENING, INPUT);                  // Hardware pulldown required
    pinMode(INPUTPIN_LIMIT_CLOSING, INPUT);                  // Hardware pulldown required

    pinMode(OUTPUTPIN_LAMP, OUTPUT);
    pinMode(OUTPUTPIN_OPENING, OUTPUT);
    pinMode(OUTPUTPIN_CLOSING, OUTPUT);
*/
    pinMode(USART_DISABLE_PIN, INPUT_PULLUP);  // Hardware pulldown required
    pinMode(DEBUG_PIN, OUTPUT);
    digitalWrite(DEBUG_PIN, ledToggle); 

    
    disableUsart=digitalRead(USART_DISABLE_PIN) == LOW;
    //Serial.begin(9600);
    //Serial.println("HI");
}

void loop()
{ 
 if (disableUsart) {
    digitalWrite(DEBUG_PIN, LOW);
    delay(300); 
    digitalWrite(DEBUG_PIN, HIGH); 
    delay(300); 
  } else {
  
    // Here we start to play
    EXECUTEFAST() {                     
        UPDATEFAST();   

        // Execute every 510ms the logic, the command to open and close the garage door
        // cames directly from SoulissApp or the push-button located on the other node
        FAST_210ms() {
/*

            PullData(Gateway_RS485, GARAGEDOOR_NODE1, GARAGEDOOR_NODE1, 1);
            readVal=mInput(GARAGEDOOR_NODE1);
            ResetInput(GARAGEDOOR_NODE1);
            
            if (readVal==8) {
              ledToggle= !ledToggle;
            }
            
            if (ledToggle) {
                digitalWrite(DEBUG_PIN, HIGH); 
            } else {
                digitalWrite(DEBUG_PIN, LOW);
            }
            */

            if(subscribe(GeneralEvent)){
            //if(subscribe(MainLedOn)){
              digitalWrite(DEBUG_PIN, HIGH); 
            }
            if(subscribe(Alarm)){
            //if(subscribe(MainLedOff)){
              digitalWrite(DEBUG_PIN, LOW); 
            }
        }

        // Process the communication, this include the command that are coming from SoulissApp
        // or from the push-button located on the other node
        FAST_PeerComms();
        
    }
    
    EXECUTESLOW() { 
        UPDATESLOW();

        SLOW_10s() {               
            // Time out commands if no limit switches are received
            Timer_GarageDoor(GARAGEDOOR_NODE1);
        } 
    }   

  }
} 
