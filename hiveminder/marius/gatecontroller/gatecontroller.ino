#include <EEPROM.h>
#include "lib.h"


/* 

  Hall effect sensor pinout when you look at the sensor and can see inscription and angled face, `pins are numbered from left to right 1 to 3
  pin 1 => VCC - 5v from arduino
  pin 2 => GND - ground from arduino
  pin 3 => data line - pin A0 on arduino

  Used transistors (2N394331) for pulling gate controller pins to ground

  note  frequency
  c     262 Hz
  d     294 Hz
  e     330 Hz
  f     349 Hz
  g     392 Hz
  a     440 Hz
  b     494 Hz
  C     523 Hz
*/


const int programModePin = 4; // Jumper closed (shorted to ground) = normal mode; Jumper open = program mode
const int statusLedPin = 13; // Off => normal; On => writing program; Flickering => programming mode; Flickering fast => error
const int gateStatusLedPin = 10; // Off => gate closed; On => gate fully open; Flickering => gate half open; Flickering fast => error
const int buzzerPin = 12;

const int sensorCount = 2;
const int triggersPerSensor = 2;

const int triggerCount = sensorCount * triggersPerSensor;
const bool singleOpenCloseSensor = sensorCount == triggerCount;
//const int sensorAndTriggerCount = 4;
const int triggerPinsPerTrigger = 2; //number of pins to trigger, useful if you want to trigger leds along with the real triggers
const int irSensorCount = 0;//2;
String triggerNames[triggerCount] = { "MASTER_CLOSE", "MASTER_OPEN", "SLAVE_CLOSE", "SLAVE_OPEN" }; // first close then open
String sensorNames[sensorCount] = { "MASTER", "SLAVE" }; //=triggerNames;
boolean closedTriggers[] = {true, false, true, false}; // if the true ones are triggered the gate is closed

// This is a 2 dimentional array so we can keep the leds if we so choose..
const int triggerPins[triggerCount][triggerPinsPerTrigger] =
{{3, 19}, {2, 11}, {9, 5}, {8, 18}}; // correct trigger pins; MC=blue MO=green SC=brown SO=orange
//{19=A5, 11, 5, 18=A4}; // trigger pins used for debugging
const int sensorPins[sensorCount] = {A3, A1}; //{A3, A2, A1, A0};
const int triggerTones[triggerCount] = {262, 524, 294, 588}; // master is lower than slave, close is lower than open

const int irSensorPins[irSensorCount] = {};// {6,7};

//int sensorValues[sensorCount];
int initSensorValues[sensorCount];
int sensorMinValues[sensorCount];
int sensorMaxValues[sensorCount];
int sensorRawValues[sensorCount];
int triggerValues[triggerCount];
float sensorRateOfChange[sensorCount];
boolean triggerTriggered[triggerCount];
boolean gateOpen = true;
boolean motionDetected = false;

const int eepromOffset = 0; // we start here then write the min values and then max values

const int maxValue = 501;
const int mainLoopDelayMilliSeconds = 10; // 1000;
const int programDetectEveryMilliSeconds = 400;
const int errorAutoResetTimoutMilliSeconds = 15000;
const int programErrorAutoResetTimoutMilliSeconds = 3000;
const int flickerMs = 1000;
const int fastFlickerMs = 400;

const int minTriggerThreshold = 45;

Mode mode;// = normal;

long flickerCounterMilliSeconds = 0;
int errorMilliSeconds = 0;

void setup() {
  Serial.begin(9600);

  pinMode(statusLedPin, OUTPUT);
  pinMode(gateStatusLedPin, OUTPUT);
  pinMode(programModePin, INPUT_PULLUP);
  pinMode(buzzerPin, OUTPUT);

  for (int triggerIndex = 0; triggerIndex < triggerCount; triggerIndex++) {
    for (int tm = 0; tm < triggerPinsPerTrigger; tm++) {
      pinMode(triggerPins[triggerIndex][tm], OUTPUT);
    }
  }

  for (int pinIndex = 0; pinIndex < irSensorCount; pinIndex++) {
    pinMode(irSensorPins[pinIndex], INPUT);
  }

  digitalWrite(statusLedPin, HIGH);
  digitalWrite(gateStatusLedPin, HIGH);
  loadSettings();
}

void loop() {
  if (mode != error && mode != programming_error && flickerCounterMilliSeconds % programDetectEveryMilliSeconds == 0) {
    //printMode();

    //detect program mode
    if (digitalRead(programModePin) == LOW) {
      if (mode == program) {
        digitalWrite(statusLedPin, HIGH);
        setMode(normal);
        if (checkSettings(true)) {
          saveSettings();
          loadSettings();
        }
      }
    } else {
      if (mode != program) {
        initProgramMode();
      }
      setMode(program);
    }
  }

  switch (mode) {
    case program:
      flicker(statusLedPin, flickerMs);
      collectProgrammingData();
      break;
    case normal:
      digitalWrite(statusLedPin, LOW);
      monitor();
      if (gateOpen) {
        flickerBuzzer(10000, 880, 100);
      }
      if (motionDetected) {
        flickerBuzzer(1000, 440, 200);
      }
      break;
    case error:
      flickerBuzzer(100, 1760, 100);
      errorFlickerAndReset(errorAutoResetTimoutMilliSeconds);
      monitor();
      break;
    case programming_error:
      flickerBuzzer(1000, 1760, 100);
      errorFlickerAndReset(programErrorAutoResetTimoutMilliSeconds);
      monitor();
      break;
    default:
      break;
  }
  delay(mainLoopDelayMilliSeconds);

  flickerCounterMilliSeconds += mainLoopDelayMilliSeconds;
  if (flickerCounterMilliSeconds < 0) { // overflow protection :(
    flickerCounterMilliSeconds = 0;
  }
}

void monitor() {
  readSensors(false, true);
  boolean oldGateOpen = gateOpen;
  gateOpen = false;
  for (int triggerIndex = 0; triggerIndex < triggerCount; triggerIndex++) {
    int sensorIndex = triggerIndexToSensorIndex(triggerIndex);
    boolean oldTriggerTriggered = triggerTriggered[triggerIndex];
    boolean newTriggerTriggered = hasTriggered(sensorRawValues[sensorIndex], triggerValues[triggerIndex], oldTriggerTriggered);

    if (newTriggerTriggered != oldTriggerTriggered) {
      triggerTriggered[triggerIndex] = newTriggerTriggered;
      Serial.print("Trigger ");
      Serial.print(triggerNames[triggerIndex]);
      Serial.print("(sensorRawValue=");
      Serial.print(sensorRawValues[sensorIndex]);
      Serial.print(") : newTriggerTriggered=");
      Serial.println(newTriggerTriggered);

      if (newTriggerTriggered) {
        for (int tm = 0; tm < triggerPinsPerTrigger; tm++) {
          digitalWrite(triggerPins[triggerIndex][tm], HIGH);
        }
        tone(buzzerPin, triggerTones[triggerIndex], 250);
      } else {
        for (int tm = 0; tm < triggerPinsPerTrigger; tm++) {
          digitalWrite(triggerPins[triggerIndex][tm], LOW);
        }
        tone(buzzerPin, triggerTones[triggerIndex], 500);
      }
    }
    if (closedTriggers[triggerIndex] && !newTriggerTriggered) {
      gateOpen = true;
    }
  }
  if (gateOpen != oldGateOpen) {
    digitalWrite(gateStatusLedPin, gateOpen);
  }

  motionDetected = false;
  for (int irSensorIndex = 0; irSensorIndex < irSensorCount; irSensorIndex++) {
    int irStatus = digitalRead(irSensorPins[irSensorIndex]);
    Serial.print("irStatus(");
    Serial.print(irSensorIndex);
    Serial.print(")=");
    Serial.println(irStatus);
    if (irStatus == HIGH) {
      motionDetected = true;
    }
  }
}

void initProgramMode() {
  readSensors(true, false);
  for (int sensorIndex = 0; sensorIndex < sensorCount; sensorIndex++) {
    sensorMinValues[sensorIndex] = MIDVAL;
    sensorMaxValues[sensorIndex] = MIDVAL;
    initSensorValues[sensorIndex] = sensorRawValues[sensorIndex];    
  }
  for (int triggerIndex = 0; triggerIndex < triggerCount; triggerIndex++) {
    triggerValues[triggerIndex] = MIDVAL;
  }
}

void collectProgrammingData() {
  // read all values
  readSensors(true, false);

  for (int triggerIndex = 0; triggerIndex < triggerCount; triggerIndex++) {
    int sensorIndex = triggerIndexToSensorIndex(triggerIndex);
    int rawValue = sensorRawValues[sensorIndex];
    int magnatude = calcMagnatude(rawValue);
    if (rawValue < sensorMinValues[sensorIndex]) {
      if (magnatude > minTriggerThreshold) {
        tone(buzzerPin, triggerTones[triggerIndex], 50);
        delay(50); // always play something of a tone..
      }
      sensorMinValues[sensorIndex] = sensorRawValues[sensorIndex];
    }
    if (rawValue > sensorMaxValues[sensorIndex]) {
      if (magnatude > minTriggerThreshold) {
        tone(buzzerPin, triggerTones[triggerIndex], 50);
        delay(50); // always play something of a tone..
      }
      sensorMaxValues[sensorIndex] = sensorRawValues[sensorIndex];
    }
    boolean isCloseTrigger = closedTriggers[triggerIndex];
    //boolean closerToInit = abs(rawValue - initSensorValues[sensorIndex]) < MIDVAL;
    boolean closerToInit = sameSideOfMidval(rawValue, initSensorValues[sensorIndex]);

    Serial.print("[");
    Serial.print(triggerIndex);
    Serial.print("](magnatude=");
    Serial.print(magnatude);
    Serial.print(",isCloseTrigger=");
    Serial.print(isCloseTrigger);
    Serial.print(",closerToInit=");
    Serial.print(closerToInit);
    Serial.print(",rawValue=");
    Serial.print(rawValue);    
    

    if (isCloseTrigger == closerToInit) {
      int oldMagnatude = calcMagnatude(triggerValues[triggerIndex]);
      Serial.print(",oldMagnatude=");
      Serial.print(oldMagnatude); 
      if (magnatude > oldMagnatude) {
        triggerValues[triggerIndex] = rawValue;
      }      
    }

    Serial.println("), ");
    
    if (!singleOpenCloseSensor || sameSideOfMidval(rawValue, initSensorValues[sensorIndex])) {
      // if single sensor, then if same side of MIDVAL as init value  then if magnitude is greater than minTriggerT

      if (magnatude > minTriggerThreshold) {
        for (int tm = 0; tm < triggerPinsPerTrigger; tm++) {
          digitalWrite(triggerPins[triggerIndex][tm], HIGH);
        }
        flickerBuzzer(((MIDVAL - magnatude) / mainLoopDelayMilliSeconds) * mainLoopDelayMilliSeconds, triggerTones[triggerIndex], 20);
      } else {
        for (int tm = 0; tm < triggerPinsPerTrigger; tm++) {
          digitalWrite(triggerPins[triggerIndex][tm], LOW);
        }
      }
    }    
  }
}


void loadSettings() {
  // read limits from EEPROM
  for (int i = 0; i < triggerCount; i++) {
    triggerValues[i] = eepromReadInt(eepromOffset + 2 * i);
  }
  //checkSettings(false);
}

void initLeds() {
  for (int triggerIndex = 0; triggerIndex < triggerCount; triggerIndex++) {
    for (int tm = 0; tm < triggerPinsPerTrigger; tm++) {
      digitalWrite(triggerPins[triggerIndex][tm], LOW);
    }
  }
}

void saveSettings() {
  tone(buzzerPin, 880, 250);
  Serial.println("Saving trigger values");
  eepromWriteIntArrayIfNeeded(eepromOffset, triggerCount, triggerValues);
  Serial.println("Done Saving trigger values");

  // reset everything
  initLeds();

  delay(200);
  tone(buzzerPin, 880, 100);
  delay(100);
}

boolean checkSettings(boolean failOnError) {
  Serial.println("Ranges: ");
  for (int triggerIndex = 0; triggerIndex < triggerCount; triggerIndex++) {
    int sensorIndex = triggerIndexToSensorIndex(triggerIndex);
    int range = sensorMaxValues[sensorIndex] - sensorMinValues[sensorIndex];
    Serial.print("[triggerIndex=");
    Serial.print(triggerIndex);
    Serial.print(",sensorIndex=");
    Serial.print(sensorIndex);
    Serial.print("](min=");
    Serial.print(sensorMinValues[sensorIndex]);
    Serial.print(",max=");
    Serial.print(sensorMaxValues[sensorIndex]);
    Serial.print(",range=");
    Serial.print(range);
    Serial.print(",triggerValue=");
    Serial.print(triggerValues[triggerIndex]);    
    Serial.println("), ");

    if (range < minTriggerThreshold || range > 1100) {
      Serial.print("Error Sensor Range doesn't look normal ");
      Serial.print(sensorNames[sensorIndex]);
      Serial.print(" : ");
      Serial.println(range);
      if (failOnError) {
        setMode(programming_error);
        return false;
      }
    }
  }
  Serial.println("");
  return true;
}


void readSensors(boolean printReading, boolean detectTampering) {
  if (printReading) {
    Serial.print("    readSensors: ");
  }
  for (int sensorIndex = 0; sensorIndex < sensorCount; sensorIndex++) {
    int rawValue = analogRead(sensorPins[sensorIndex]);
    int magnatude = calcMagnatude(rawValue);
    int oldRaw = sensorRawValues[sensorIndex];
    sensorRateOfChange[sensorIndex] = (sensorRateOfChange[sensorIndex] + abs(oldRaw - rawValue)) * 0.995;// 0.9;

    sensorRawValues[sensorIndex] = rawValue;

    if (printReading) {
      Serial.print(magnatude);
      Serial.print(" rawValue(");
      Serial.print(rawValue);
      Serial.print(") c[");
      Serial.print(sensorRateOfChange[sensorIndex]);
      Serial.print("], ");
    }

    // malfunction and tamper protection
    if (detectTampering && (rawValue == 0 || rawValue == 1023 || sensorRateOfChange[sensorIndex] > 2500)) {
      Serial.print("Error Sensor ");
      Serial.print(sensorNames[sensorIndex]);
      Serial.print("[");
      Serial.print(sensorIndex);
      Serial.print("]:  |");
      Serial.print(rawValue);
      Serial.print(" - 512| = ");
      Serial.print(magnatude);
      Serial.print(" ~ ");
      Serial.println(sensorRateOfChange[sensorIndex]);
      sensorRateOfChange[sensorIndex] = 0; // make reset possible
      setMode(error);
    }
  }
  if (printReading) {
    Serial.println();
  }
}

// assumes per sensor first close trigger then open trigger
int sensorIndexToTriggerIndex(int sensorIndex, boolean closeTrigger) {
  if (singleOpenCloseSensor) {
    return sensorIndex;
  } else {
    return sensorIndex + (closeTrigger?0:1);
  }
}

// assumes first one sensor then the other..
int triggerIndexToSensorIndex(int triggerIndex) {
  if (singleOpenCloseSensor) {
    return triggerIndex;
  } else {
    return triggerIndex / triggersPerSensor;
  }
}


void errorFlickerAndReset(int errorAutoResetTimoutMilliSeconds) {
  flicker(statusLedPin, fastFlickerMs);
  flicker(gateStatusLedPin, fastFlickerMs);

  errorMilliSeconds += mainLoopDelayMilliSeconds;
  if (errorMilliSeconds >= errorAutoResetTimoutMilliSeconds) {
    setMode(normal);
  }
}

void flicker(int ledPin, int intervalMilliSeconds) {
  intervalMilliSeconds = (intervalMilliSeconds / mainLoopDelayMilliSeconds) * mainLoopDelayMilliSeconds; // must be multiple
  if (flickerCounterMilliSeconds % intervalMilliSeconds == 0) {
    if ((flickerCounterMilliSeconds / intervalMilliSeconds) % 2 == 0) {
      digitalWrite(ledPin, HIGH);
    } else {
      digitalWrite(ledPin, LOW);
    }
  }
}

void flickerBuzzer(int intervalMilliSeconds, int freqency, int duration) {
  intervalMilliSeconds = (intervalMilliSeconds / mainLoopDelayMilliSeconds) * mainLoopDelayMilliSeconds; // must be multiple
  if (flickerCounterMilliSeconds % intervalMilliSeconds == 0) {
    tone(buzzerPin, freqency, duration);
    delay(50); // always play something of a tone..
  }
}

void setMode(Mode newMode) {
  if (mode != newMode) {
    mode = newMode;
    if (mode == error) {
      errorMilliSeconds = 0;
    }
    if (mode == normal) {
      initLeds();
    }
    printMode();
  }
}

void printMode() {
  Serial.print("Mode: ");
  switch (mode) {
    case program:
      Serial.println("program");
      break;
    case normal:
      Serial.println("normal");
      break;
    case error:
      Serial.println("error");
      break;
    default:
      Serial.println("unknown");
      break;
  }
}

