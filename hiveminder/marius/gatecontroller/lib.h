#include <EEPROM.h> //Needed to access the eeprom read write functions
#include <Arduino.h>

// define enum here so it can be used in function defs
// http://forum.arduino.cc/index.php?topic=68701.msg507200#msg507200

enum Mode {
  normal,
  program, 
  error,
  programming_error
};

const int MIDVAL = 512;
const int antiBounceThreshold = 20; // this should be a percentage of magnatude I think?!

int calcMagnatude(int rawValue) {
  return abs(rawValue - MIDVAL); // make it absolute, and don't worry about value flips close to MIDVAL    
}

boolean hasTriggered(int sensorRawValue, int triggerValue, boolean oldTriggerTriggered) {
  int grace = oldTriggerTriggered ? antiBounceThreshold:4;
  //Serial.print("grace =");
  //Serial.println(grace);
  return abs(sensorRawValue - triggerValue) <= grace;
  /*
  if (triggerValue > MIDVAL) {
    return sensorRawValue >= triggerValue - grace;
  } else {
    return sensorRawValue <= triggerValue + grace;
  }*/
}

boolean sameSideOfMidval(int a, int b) {
  return (a <= MIDVAL && b <= MIDVAL) || (a > MIDVAL && b > MIDVAL);
}

//
// EEPROM utils
//

//adapted from here: http://forum.arduino.cc/index.php?topic=37470.0
//This function will write a 2 byte integer to the eeprom at the specified address and address + 1
void eepromWriteInt(int p_address, int p_value){  
  byte lowByte1 = (p_value & 0xFF);
  byte highByte1 = ((p_value >> 8) & 0xFF);

  Serial.print("writeing: ");
  Serial.print(p_address);
  Serial.print(", ");
  Serial.print(lowByte1, DEC);
  Serial.print(", ");
  Serial.println(highByte1, DEC);
  
  EEPROM.write(p_address, lowByte1);
  EEPROM.write(p_address + 1, highByte1);
  //delay(4);
}

//This function will read a 2 byte integer from the eeprom at the specified address and address + 1
unsigned int eepromReadInt(int p_address){
  byte lowByte1 = EEPROM.read(p_address);
  byte highByte1 = EEPROM.read(p_address + 1);
  Serial.print("reading: ");
  Serial.print(p_address);
  Serial.print(", ");
  Serial.print(lowByte1, DEC);
  Serial.print(", ");
  Serial.println(highByte1, DEC);
  
  return (lowByte1 & 0xFF) | ((highByte1 << 8) & 0xFF00);
}

void eepromWriteIntIfNeeded(int p_address, int newValue){  
  int oldValue = eepromReadInt(p_address);
  if (newValue != oldValue){    
    Serial.print("writing: ");
    Serial.print(p_address);
    Serial.print(" = ");
    Serial.print(newValue);
    Serial.print(" was: ");
    Serial.print(oldValue);
    eepromWriteInt(p_address, newValue);    
delay(25);
    Serial.print(" now: ");
    Serial.println(eepromReadInt(p_address));
  }        
}

void eepromWriteIntArrayIfNeeded(int p_address, int valueCount, int values[]){  
  for (int i = 0; i < valueCount; i++) {
    eepromWriteIntIfNeeded(p_address + 2 * i, values[i]);
  }
}

void eepromReadIntArray(int p_address, int valueCount, int *values){  
  for (int i = 0; i < valueCount; i++) {
    values[i] = eepromReadInt(p_address + 2 * i);
  }
}


