// Example for using AH3503 SERIES LINEAR HALL-EFFECT SENSOR
// Datasheet: https://opencircuit.nl/ProductInfo/1000037/AH3503.pdf

// adapted from Hest's sample: http://forum.arduino.cc/index.php?topic=284041.msg1991678#msg1991678

// simple example that figures out a baseline and then write out serial info when data change, 
// it also flashes the built in LED while calabrating and when there is a signifigant difference
// with the baseline.

// pinout when you look at the sensor and can see inscription and angled face, 
// pins are numbered from left to right 1 to 3
// pin 1 => VCC - 5v from arduino
// pin 2 => GND - ground from arduino
// pin 3 => data line - pin A0 on arduino

// no other components like resistors etc. were required

int mid;
int diff = 2;					
int raw;
int rawMin = 1024;
int rawMax = 0;
int calruns = 10;
unsigned long calibration = 0;
int debugLed = 13;
int readPin = A0;

void setup() 
{
	Serial.begin(9600);
        pinMode(debugLed, OUTPUT);     
	findmid();
}

void Measurement()
{
	raw = analogRead(readPin);   // Range : 0..1024
//Serial.print("raw: ");
//Serial.println(raw);
        rawMin = min(raw, rawMin);
        rawMax = max(raw, rawMax);        
        Serial.print("Output: ");
        Serial.print(raw);
        Serial.print(" - ");
        Serial.print(mid);
        Serial.print(" = ");                
  	Serial.print(raw-mid);  
        Serial.print(" [");                
        Serial.print(rawMin);        
        Serial.print(" , ");
        Serial.print(rawMax);                
        Serial.println("]");        

	if(raw < mid-diff || raw > mid+diff)  {
                digitalWrite(debugLed, HIGH);
				
	} else {
                digitalWrite(debugLed, LOW);
        }        
}

void findmid() {
        digitalWrite(debugLed, HIGH);
	delay(100);
	Serial.println("Setting up... Please wait");
	for(int i = 0; i < calruns; i++)
	{
		raw = analogRead(readPin);
		calibration = calibration + raw;

                digitalWrite(debugLed, !digitalRead(debugLed));
		delay(200);
	}
	mid = calibration / calruns;
	Serial.print("Midpoint found at ");
	Serial.println(mid);
	Serial.println("Ready:");
        digitalWrite(debugLed, LOW);
}

void loop() 
{
    delay(125);
    Measurement();
}
