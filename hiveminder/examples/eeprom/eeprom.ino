#include <EEPROM.h>

# Example on how a 2 byte integer can be stored into eeprom

int addr = 10;

void setup()
{  
  Serial.begin(9600);
 
  int val = 1022; 
  //int val = analogRead(0); // for a random value
  Serial.println(val, DEC);
  
  byte lowByte1 = (byte)(val & 0xFF);
  byte highByte1 = (byte)((val >> 8) & 0xFF);
  Serial.println(lowByte1, DEC);
  Serial.println(highByte1, DEC);
  EEPROM.write(addr+2, highByte1 );
  EEPROM.write(addr,lowByte1 );
  
  delay(100);
  
  byte lowByte2 = EEPROM.read(addr);
  byte highByte2 = EEPROM.read(addr + 2);
  
  Serial.println(lowByte2, DEC);
  Serial.println(highByte2, DEC);
  int value = lowByte2 | highByte2 << 8;
  Serial.println(value, DEC);
 
}

void loop()
{

}
