

// a more usable linear hall effect sampl

// if programModePin is pulled up (10k resistor to VCC) on startup, programming mode is entered, else normal mode is entered.
// (there is a little lag from after the jumper is removed until the correct mode is selected when restarting)

enum Mode {
  normal,
  program, 
  error
};

const int programModePin = 5;
const int lhesLedPin0 = 10; // linear halll effect sensor led 0 on pin 10
const int maxValue = 501;
const int errorAutoResetTimoutSeconds = 15;

Mode mode = normal;

// monitoring and tracking
int rateOfChange = 0;
int errorSeconds = 0;

int mid = 526;
int threshold = 2;					
int raw;
int oldRaw;
int calruns = 10;
unsigned long calibration = 0;
int debugLed = 13;

void setup() 
{
  Serial.begin(9600);
  
  pinMode(debugLed, OUTPUT);     
  pinMode(lhesLedPin0, OUTPUT);
  pinMode(programModePin, INPUT);
  
  if (digitalRead(programModePin) == LOW) {
    mode = normal;      
    analogWrite(lhesLedPin0, 255); 
    //findmid();  
  } else {
    mode = program;
    analogWrite(lhesLedPin0, 127);     
  }
  
  Serial.print("Mode: ");
  Serial.println(mode);
  
  

}

void loop() 
{
    delay(25);    
    switch ( mode ) {
          case program:
              Measurement();    
              break;               
          case normal:              
              Measurement();
              break;  
          case error:
              digitalWrite(debugLed, HIGH);
              digitalWrite(lhesLedPin0, LOW);              
              
              delay(1000);
              digitalWrite(debugLed, LOW);
              digitalWrite(lhesLedPin0, HIGH);                            
              delay(1000);
              
              errorSeconds+=2;
              Serial.print("Error seconds: ");
              Serial.println(errorSeconds);
              if (errorSeconds >= errorAutoResetTimoutSeconds) {
                mode = normal;
                rateOfChange = 0;
                //findmid();
              }
              break;     
          default:
              break;
      }
}

void Measurement() {
        oldRaw = raw;
	raw = analogRead(0);   // Range : 0..1024
        rateOfChange = (rateOfChange + abs(oldRaw - raw)) * 0.9;
        int diff = abs(raw-mid);
        analogWrite(lhesLedPin0, 255 - min(diff * 9, 255) );
        
        if (raw == 0 || raw == 1023 || rateOfChange > 800) { // malfunction and tamper protection
          mode = error;
          errorSeconds = 0;
          Serial.print("Error: ");
          Serial.print(raw);
          Serial.print(" - ");
          Serial.print(mid);
          Serial.print(" = ");                
  	  Serial.print(raw-mid);
          Serial.print(" ~ ");
	  Serial.println(rateOfChange);
        }
        
//Serial.print("raw: ");
//Serial.println(raw);
        
	//if(diff > threshold)  {
//        if( (maxValue < mid && raw <= maxValue) || (maxValue > mid && raw >= maxValue)){
        if( diff >= 26){  
                digitalWrite(debugLed, LOW);
		
	} else {
                digitalWrite(debugLed, HIGH);
        }       
        if( diff >= 26 || mode == program){      
                Serial.print("Output: ");
                Serial.print(raw);
                Serial.print(" - ");
                Serial.print(mid);
                Serial.print(" = ");                
		Serial.print(raw-mid);
                Serial.print(" ~ ");
		Serial.println(rateOfChange);
        }
}

void findmid() {
        calibration = 0;
        digitalWrite(debugLed, HIGH);
	delay(100);
	Serial.println("Setting up... Please wait");
	for(int i = 0; i < calruns; i++)
	{
		raw = analogRead(A0);
		calibration = calibration + raw;

                digitalWrite(debugLed, !digitalRead(13));
		delay(200);
	}
	mid = calibration / calruns;
	Serial.print("Midpoint found at ");
	Serial.println(mid);
	Serial.println("Ready:");
        digitalWrite(debugLed, LOW);
}


