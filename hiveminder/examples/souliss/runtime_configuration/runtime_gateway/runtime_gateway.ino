/**************************************************************************
    Souliss - Gateway        
***************************************************************************/

/*

ethENC28J60 pinout on mini (unstable)
10 => CS
11 => SI
12 => SO
13 => SCK

on mega (more stable!)
10 => CS
51 => SI
50 => SO
52 => SCK

 */
/*#define SERIALPORT_INSKETCH
#define LOG          Serial
#define SOULISS_DEBUG 1
#define MaCaco_DEBUG   1
#define VNET_DEBUG    1*/

#include "SoulissFramework.h"

// Include framework code and libraries
#include <SPI.h>
#include <EEPROM.h>
#include "bconf/StandardArduino.h"
#include "conf/ethENC28J60.h"
#include "conf/usart.h"                     // USART RS485
//#include "conf/Gateway.h"           
#include "conf/RuntimeGateway.h"            // This node is a Peer and can became a Gateway at runtime


#include "conf/DynamicAddressing.h"         // Use dynamic addresses
#include "conf/IPBroadcast.h"               // Use IP Broadcast for Ethernet/WiFi nodes without a static address


/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.
//
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           4
#define USARTDRIVER             Serial


/*** All configuration includes should be above this line ***/ 
#include "Souliss.h"
       
// Define the network configuration according to your router settings
uint8_t ip_address[4]  = {192, 168, 0, 3};
uint8_t subnet_mask[4] = {255, 255, 255, 0};
uint8_t ip_gateway[4]  = {192, 168, 0, 1};
#define myvNet_address  ip_address[3]       // The last byte of the IP address (77) is also the vNet address
#define myvNet_subnet   0xFF00

// Define the RS485 network configuration
//#define Gateway_RS485   0xCE01
//#define Peer_RS485      0xCE20

// This identify the number of the LED logic
#define SLOT_LIGHT1        0       
#define SLOT_PIR1          SLOT_LIGHT1 + 1
#define SLOT_PHOTORESIST1  SLOT_PIR1 + 1 // takes 2 bites

//const int generalLedPin = 13;
const int photoResistorPin = A0;
const int pirPin = 2;
const int pirLightPin = 7;
const int buttonPin = 3;
const int lightPin = 9;

void setup() {   
    //pinMode(generalLedPin, OUTPUT);
    //digitalWrite(generalLedPin, HIGH); 
    //Serial.begin(9600);
    Initialize();

     // Set network parameters
    Souliss_SetIPAddress(ip_address, subnet_mask, ip_gateway);
    SetAsGateway(myvNet_address);                                   // Set this node as gateway for SoulissApp  
    SetAddressingServer();
    /*
    SetAddress(Gateway_RS485, myvNet_subnet, 0);                    // Set the address on the RS485 bus

    // This node as gateway will get data from the Peer
    SetAsPeerNode(Peer_RS485, 1);
    SetAsPeerNode(0xCE21, 2);
    SetAsPeerNode(0xCE22, 4);
    SetAsPeerNode(0xCE03, 3);
    */
     
    Set_T11(SLOT_LIGHT1);
    Set_T13(SLOT_PIR1);
    Set_T54(SLOT_PHOTORESIST1);
    
    pinMode(pirPin, INPUT);                  // Hardware pulldown required
    pinMode(pirLightPin, OUTPUT);
    pinMode(buttonPin, INPUT_PULLUP);                
    pinMode(lightPin, OUTPUT);                 
    //digitalWrite(generalLedPin, LOW);
    //delay(500);
}

void loop() {
    EXECUTEFAST() {                     
        UPDATEFAST();   
        
        FAST_50ms() { 
            
            //DigIn2State(pirPin, Souliss_T1n_OffCmd, Souliss_T1n_OnCmd, SLOT_PIR1);
            // I think this prevents needless updates:
            if (digitalRead(pirPin)) {
              DigIn(pirPin, Souliss_T1n_OnCmd, SLOT_PIR1);
            } else {              
              DigIn(pirPin, Souliss_T1n_OffCmd, SLOT_PIR1);            
            }
            
            DigIn(buttonPin, Souliss_T1n_ToggleCmd, SLOT_LIGHT1); 

            AnalogIn(photoResistorPin, SLOT_PHOTORESIST1, 0.09, 0);   // The raw data is 0-1024, scaled as 0-100% without bias (100 / 1024 = 0.09)

            Logic_T13(SLOT_PIR1);
            Logic_T11(SLOT_LIGHT1);          

            DigOut(lightPin, Souliss_T1n_Coil, SLOT_LIGHT1);                // Use the pin9 to give power to the LED according to the logic
            LowDigOut(pirLightPin, Souliss_T1n_Coil, SLOT_PIR1);
        } 

        FAST_210ms() {
            // Compare the acquired input with the stored one, send the new value to the
            // user interface if the difference is greater than the dead-band
            Read_AnalogIn(SLOT_PHOTORESIST1);
        }
              
        FAST_GatewayComms();                                
    }
    
    EXECUTESLOW() {
        UPDATESLOW();

        SLOW_10s() {
        }
    }
    
} 
