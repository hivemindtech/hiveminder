/**************************************************************************
    Souliss - slave RS485/USART
***************************************************************************/
/*
#define SERIALPORT_INSKETCH
#define LOG          Serial
#define SOULISS_DEBUG 1
#define MaCaco_DEBUG   1
#define VNET_DEBUG    1
*/

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/usart.h"                     // USART RS485

/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.
//
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           4
#define USARTDRIVER             Serial
/*************/

// Include framework code and libraries
#include <SPI.h>
#include <EEPROM.h>

/*** All configuration includes should be above this line ***/
#include "Souliss.h"

// Define the RS485 network configuration
//#define myvNet_subnet   0xFF00
//#define Gateway_RS485   0xCE01
//#define Peer_RS485      0xCE22

// this is the 'slot' memory address
// https://github.com/souliss/souliss/wiki/SoulissAPI
// https://github.com/souliss/souliss/wiki/Data%20Structure
#define SLOT_LIGHT1        0

#define USART_DISABLE_PIN 8 // bridge this to ground to disable comms, for in case :)
boolean disableUsart;

const int generalLedPin = 13;
const int buttonPin = 7;
const int lightPin = 9;
int readVal = 123;
boolean ledToggle = true;

void setup() {
  pinMode(generalLedPin, OUTPUT);
  pinMode(lightPin, OUTPUT);
  pinMode(USART_DISABLE_PIN, INPUT_PULLUP);
  pinMode(buttonPin, INPUT_PULLUP);

  digitalWrite(generalLedPin, HIGH);
  disableUsart = digitalRead(USART_DISABLE_PIN) == LOW;

  //Serial.begin(9600);
  Initialize();

  // Set network parameters
  //SetAddress(Peer_RS485, myvNet_subnet, Gateway_RS485);
  
  // This board request an address to the gateway at runtime, no need
  // to configure any parameter here.
  SetDynamicAddressing();
  GetAddress();   


  Set_T11(SLOT_LIGHT1);

  digitalWrite(lightPin, ledToggle);
  digitalWrite(generalLedPin, LOW);
}

void loop() {
  if (disableUsart) {
    digitalWrite(generalLedPin, LOW);
    delay(300);
    digitalWrite(generalLedPin, HIGH);
    delay(300);
  } else {

    // Here we start to play
    EXECUTEFAST() {
      UPDATEFAST();

      // Execute every 510ms the logic, the command to open and close the garage door
      // cames directly from SoulissApp or the push-button located on the other node
      FAST_110ms() {

        DigIn(buttonPin, Souliss_T1n_ToggleCmd, SLOT_LIGHT1);
        Logic_T11(SLOT_LIGHT1);
        DigOut(lightPin, Souliss_T1n_Coil, SLOT_LIGHT1);
      }

      // Process the communication, this include the command that are coming from SoulissApp
      // or from the push-button located on the other node
      FAST_PeerComms();
    }

    EXECUTESLOW() {
      UPDATESLOW();
      SLOW_10s() {
      }
      // Here we periodically check for a gateway to join
      SLOW_PeerJoin(); 
    }

  }
}
