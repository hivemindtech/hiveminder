/**************************************************************************
    Souliss - slave RS485/USART                
***************************************************************************/
/*
#define SERIALPORT_INSKETCH
#define LOG          Serial
#define SOULISS_DEBUG 1
#define MaCaco_DEBUG   1
#define VNET_DEBUG    1
*/

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/usart.h"                     // USART RS485

/*************/
// Use the following if you are using an RS485 transceiver with 
// transmission enable pin, otherwise delete this section.
//
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           4
#define USARTDRIVER             Serial
/*************/

// Include framework code and libraries
#include <SPI.h>

/*** All configuration includes should be above this line ***/ 
#include "Souliss.h"

// Define the RS485 network configuration
#define myvNet_subnet   0xFF00
#define Gateway_RS485   0xCE01
#define Peer_RS485      0xCE32

// this is the 'slot' memory address
// https://github.com/souliss/souliss/wiki/SoulissAPI
// https://github.com/souliss/souliss/wiki/Data%20Structure
#define SLOT_LIGHT1        0                 
#define SLOT_PIR1          SLOT_LIGHT1 + 1
#define SLOT_PHOTORESIST1  SLOT_PIR1 + 1 // takes 2 bytes
#define SLOT_LINEAR_HALL_EFFECT1  SLOT_PHOTORESIST1 + 2 // takes 2 bytes

#define USART_DISABLE_PIN 2 // bridge this to ground to disable comms, for in case :)
boolean disableUsart;

const int generalLedPin = 13;
const int photoResistorPin = A0; // 5v -> photoresistor -> (A0) & (1kohm -> ground)
const int pirPin = 3;
const int pirLightPin = 7;
const int buttonPin = 8;  // pin -> button -> ground
const int lightPin = 9;
const int linearHallEffectPin = A3;

void setup() {   
    pinMode(generalLedPin, OUTPUT);      
    pinMode(USART_DISABLE_PIN, INPUT_PULLUP);    
    pinMode(lightPin, OUTPUT);
    pinMode(pirPin, INPUT);
    pinMode(pirLightPin, OUTPUT);
    pinMode(buttonPin, INPUT_PULLUP); 

    digitalWrite(generalLedPin, HIGH); 
    disableUsart=digitalRead(USART_DISABLE_PIN) == LOW;

    //Serial.begin(9600);
    Initialize();
    
    // Set network parameters
    SetAddress(Peer_RS485, myvNet_subnet, Gateway_RS485);  

    Set_T11(SLOT_LIGHT1);
    Set_T13(SLOT_PIR1);
    Set_T54(SLOT_PHOTORESIST1);
    Set_T51(SLOT_LINEAR_HALL_EFFECT1);    
 
    digitalWrite(generalLedPin, LOW);
}

void loop() { 
 if (disableUsart) {
    digitalWrite(generalLedPin, LOW);
    delay(300); 
    digitalWrite(generalLedPin, HIGH); 
    delay(300); 
  } else {
  
    // Here we start to play
    EXECUTEFAST() {                     
        UPDATEFAST();   

        // Execute every 510ms the logic, the command to open and close the garage door
        // cames directly from SoulissApp or the push-button located on the other node
        FAST_110ms() {

            DigIn2State(pirPin, Souliss_T1n_OffCmd, Souliss_T1n_OnCmd, SLOT_PIR1);
            DigIn(buttonPin, Souliss_T1n_ToggleCmd, SLOT_LIGHT1);
            AnalogIn(photoResistorPin, SLOT_PHOTORESIST1, 0.09, 0);   // The raw data is 0-1024, scaled as 0-100% without bias (100 / 1024 = 0.09)
            AnalogIn(linearHallEffectPin, SLOT_LINEAR_HALL_EFFECT1, 0.09, 0);   // The raw data is 0-1024, scaled as 0-100% without bias (100 / 1024 = 0.09)

            Logic_T13(SLOT_PIR1);
            Logic_T11(SLOT_LIGHT1);  
            
            DigOut(lightPin, Souliss_T1n_Coil, SLOT_LIGHT1);  
            LowDigOut(pirLightPin, Souliss_T1n_Coil, SLOT_PIR1);           
        }
        
        FAST_210ms() {
            // Compare the acquired input with the stored one, send the new value to the
            // user interface if the difference is greater than the dead-band
            Read_AnalogIn(SLOT_PHOTORESIST1);

            Read_AnalogIn(SLOT_LINEAR_HALL_EFFECT1);
        }

        // Process the communication, this include the command that are coming from SoulissApp
        // or from the push-button located on the other node
        FAST_PeerComms();       
    }
    
    EXECUTESLOW() { 
        UPDATESLOW();
        SLOW_10s() {               
        } 
    }       
  }
}
