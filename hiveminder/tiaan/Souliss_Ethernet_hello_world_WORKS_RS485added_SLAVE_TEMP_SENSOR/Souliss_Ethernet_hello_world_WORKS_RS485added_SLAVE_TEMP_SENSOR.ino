/**************************************************************************
    Souliss - Hello World

    This is the basic example, control one LED via a push-button or Android
    using SoulissApp (get it from Play Store).

    Run this code on one of the following boards:
      - Arduino with ENC28J60 Ethernet Shield

***************************************************************************/

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/usart.h"                     // USART RS485

/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.
//
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           4
#define USARTDRIVER             Serial

// Include framework code and libraries
#include <SPI.h>

/*** All configuration includes should be above this line ***/
#include "Souliss.h"

// Define the network configuration according to your router settings

#define myvNet_subnet   0xFF00

// Define the RS485 network configuration
#define Gateway_RS485   0xCE01
#define Peer_RS485      0xCE02

// This identify the number of the LED logic
#define MYLEDLOGIC          0

// ADC settings
#define ANALOGDAQ               1           // This is the memory slot used for the execution of the logic
#define DEADBAND                0.05        // Deadband value 5%  



#define USART_DISABLE_PIN 8 // bridge this to ground to disable comms, for in case :)

boolean disableUsart;

void setup()
{
  Initialize();

  SetAddress(Peer_RS485, myvNet_subnet, Gateway_RS485);                    // Set the address on the RS485 bus


  Set_SimpleLight(MYLEDLOGIC);        // Define a simple LED light logic
  Set_AnalogIn(ANALOGDAQ);                    // Set an analog input value

  pinMode(9, OUTPUT);                 // Power the LED

  pinMode(USART_DISABLE_PIN, INPUT_PULLUP);  // Hardware pulldownle; required

  disableUsart = digitalRead(USART_DISABLE_PIN) == LOW;

}

void loop()
{
  if (disableUsart) {
    digitalWrite(9, LOW);
    delay(300);
    digitalWrite(9, HIGH);
    delay(300);
  } else {
    // Here we start to play
    EXECUTEFAST() {
      UPDATEFAST();

      FAST_110ms() {   // We process the logic and relevant input and output every 50 milliseconds
        //DigIn(2, Souliss_T1n_ToggleCmd, MYLEDLOGIC);            // Use the pin2 as ON/OFF toggle command
        Logic_SimpleLight(MYLEDLOGIC);                          // Drive the LED as per command
        DigOut(9, Souliss_T1n_Coil, MYLEDLOGIC);                // Use the pin9 to give power to the LED according to the logic
        // Compare the acquired input with the stored one, send the new value to the
        // user interface if the difference is greater than the dead-band
        Read_AnalogIn(ANALOGDAQ);

      }

      // Here we handle here the communication with Android, commands and notification
      // are automatically assigned to MYLEDLOGIC
      FAST_PeerComms();

    }
    EXECUTESLOW(){   
                UPDATESLOW();
                SLOW_10s()  {
                // Acquire data from the microcontroller ADC
                AnalogIn(A0, ANALOGDAQ, 0.09, 0);   // The raw data is 0-1024, scaled as 0-100% without bias (100 / 1024 = 0.09)
                  
                }    
        }
  }
}
