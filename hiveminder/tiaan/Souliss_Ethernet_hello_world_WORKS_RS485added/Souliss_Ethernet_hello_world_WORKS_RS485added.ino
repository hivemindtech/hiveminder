/**************************************************************************
    Souliss - Hello World

    This is the basic example, control one LED via a push-button or Android
    using SoulissApp (get it from Play Store).

    Run this code on one of the following boards:
      - Arduino with ENC28J60 Ethernet Shield

***************************************************************************/

// Configure the framework
#include "bconf/StandardArduino.h"          // Use a standard Arduino
#include "conf/ethENC28J60.h"               // Ethernet through Wiznet W5100
#include "conf/usart.h"                     // USART RS485
#include "conf/Gateway.h"                   // The main node is the Gateway, we have just one node

/*************/
// Use the following if you are using an RS485 transceiver with
// transmission enable pin, otherwise delete this section.
//
#define USARTDRIVER_INSKETCH
#define USART_TXENABLE          1
#define USART_TXENPIN           4
#define USARTDRIVER             Serial

// Include framework code and libraries
#include <SPI.h>

/*** All configuration includes should be above this line ***/
#include "Souliss.h"

// Define the network configuration according to your router settings
uint8_t ip_address[4]  = {192, 168, 0, 3};
uint8_t subnet_mask[4] = {255, 255, 255, 0};
uint8_t ip_gateway[4]  = {192, 168, 0, 1};
#define myvNet_address  ip_address[3]       // The last byte of the IP address (77) is also the vNet address
#define myvNet_subnet   0xFF00

// Define the RS485 network configuration
#define Gateway_RS485   0xCE01
//#define Peer_RS485      0xCE02

// This identify the number of the LED logic
#define MYLEDLOGIC          0



#define USART_DISABLE_PIN 8 // bridge this to ground to disable comms, for in case :)

boolean disableUsart;

void setup()
{
  pinMode(13, OUTPUT);                 // Power the LED

  pinMode(USART_DISABLE_PIN, INPUT_PULLUP);  // Hardware pulldown required

  disableUsart = digitalRead(USART_DISABLE_PIN) == LOW;

  if (disableUsart) {
    while (1) {
      digitalWrite(13, LOW);
      delay(1000);
      digitalWrite(13, HIGH);
      delay(1000);
    }

  }


  Initialize();

  // Set network parameters
  Souliss_SetIPAddress(ip_address, subnet_mask, ip_gateway);
  SetAsGateway(myvNet_address);                                   // Set this node as gateway for SoulissApp
  SetAddress(Gateway_RS485, myvNet_subnet, 0);                    // Set the address on the RS485 bus

  // This node as gateway will get data from the Peer
  //SetAsPeerNode(Peer_RS485, 1);

  Set_SimpleLight(MYLEDLOGIC);        // Define a simple LED light logic

  // We connect a pushbutton between 5V and pin2 with a pulldown resistor
  // between pin2 and GND, the LED is connected to pin9 with a resistor to
  // limit the current amount
  //pinMode(2, INPUT);                  // Hardware pulldown required


}

void loop()
{
  if (disableUsart) {
    digitalWrite(13, LOW);
    delay(1000);
    digitalWrite(13, HIGH);
    delay(1000);
  } else {
    delay(10000);
    // Here we start to play
    EXECUTEFAST() {
      UPDATEFAST();

      FAST_50ms() {   // We process the logic and relevant input and output every 50 milliseconds
        //DigIn(2, Souliss_T1n_ToggleCmd, MYLEDLOGIC);            // Use the pin2 as ON/OFF toggle command
        Logic_SimpleLight(MYLEDLOGIC);                          // Drive the LED as per command
        DigOut(9, Souliss_T1n_Coil, MYLEDLOGIC);                // Use the pin9 to give power to the LED according to the logic

      }

      // Here we handle here the communication with Android, commands and notification
      // are automatically assigned to MYLEDLOGIC
      FAST_GatewayComms();

    }

    EXECUTESLOW() {
      UPDATESLOW();
      SLOW_10s()  {
        //  Send(Peer_RS485, 0, 1);

      }
    }
  }
}
